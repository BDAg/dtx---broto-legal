import { Router } from 'express';
import { add } from '../controllers/ClientController';
import Access from '../middleware/VerifyToken';

const Route = Router();

Route.post('/', Access(3), add);

export default Route;