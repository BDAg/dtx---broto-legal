import { Router } from 'express';
import {CTRLnewLogin,CTRLverifyUser,CTRLnewData,CTRLnewUser} from '../controllers/UserController';
import Access from '../middleware/VerifyToken';

const Route = Router();

// Route.post('/', Access(3), add);
// Route.put('/:id', Access(3), edit);
// Route.get('/', Access(3), list);
// Route.get('/:id', Access(3), search);
// Route.delete('/:id', Access(3), del);

Route.post('/', CTRLnewLogin)
Route.post('/verify',CTRLverifyUser)
Route.post('/data', Access(3), CTRLnewData)
Route.put('/', Access(3), CTRLnewUser)
export default Route;
