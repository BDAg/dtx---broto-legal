import { Router } from 'express';
import { add, edit, search, del, listprojectsmembers, list } from '../controllers/ProjectController.js';
import Access from '../middleware/VerifyToken';

const Route = Router();

Route.post('/', Access(3), add);

export default Route;