import { MDLnewCLI} from '../models/ClientModel';


export function add(req, res) { 
    MDLnewCLI(req.body.nome, req.body.email, req.body.sexo,req.body.nascimento,req.body.telefone,req.body.endereco,req.body.estado,req.body.cidade).then((response) => {
        res.status(200).send ({ message: 'Cliente adicionado com sucesso!', response});
    }).catch((error) => {
        res.status(500).send ({ message: 'Erro ao adicionar cliente!', error});
    });
}
