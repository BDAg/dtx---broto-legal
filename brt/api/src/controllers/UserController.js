import {MDLnewLogin,MDLverifyUser, MDLnewData,MDLnewUser} from '../models/UserModel';
import bcrypt from 'bcryptjs';

/**
 * Controle para acionar o modelo de inserção de credenciais a um novo usuário
 * 
 * @param {*} req 
 * @param {*} res 
 */
export function CTRLnewLogin(req, res) {
    if(!req.body.email || !req.body.password){
        res.status(406).send({message: 'Informe os parametros devidamente.'});
        return;
    } 
    MDLnewLogin(req.body.email, bcrypt.hashSync(req.body.password)).then( (response) => {
        res.status(200).send({ message: 'Usuário criado com sucesso!' });
    }).catch( (error) => {
        res.status(500).send({ message: 'Houve um erro ao adicionar o usuário.', error });
    });
}

/**
 * Controle para acionar o modelo de verificação de usuário
 * 
 * @param {*} req 
 * @param {*} res 
 */
export function CTRLverifyUser(req,res){
    if(!req.body.idLoginUser){
        res.status(406).send({message: 'Informe os parametros devidamente.'});
        return;
    }     
    MDLverifyUser(req.body.idLoginUser).then((response) => {
        res.status(200).send({message: 'O usuário está confirmado'})
    }).catch((err) => {
        res.status(400).send({message: 'Não há registros deste usuário'})
    })
}

/**
 * Controle para acionar o modelo de inserção de informações de um usuário
 * @param {*} req 
 * @param {*} res 
 */
export function CTRLnewData(req,res){
    if(!req.body.first_name || !req.body.last_name){
        res.status(406).send({message: 'Informe os parametros devidamente.'});
        return;      
    }

    MDLnewData(req.body.first_name,req.body.last_name,req.body.description).then((response) => {
        res.status(200).send({ message: 'As informações foram inseridas com sucesso', id:response });
    }).catch((err) => {
        res.status(500).send({ message: 'Houve um erro ao inserir as informações', err });
    })
}

/**
 * Controle para acionar o modelo de inserção de usuário
 * 
 * @param {*} req 
 * @param {*} res 
 */
export function CTRLnewUser(req,res){
    if(!req.body.idDetailsUser || !req.body.idLoginUser){
        res.status(406).send({message: 'Informe os parametros devidamente.'});
        return;      
    }

    MDLnewUser(req.body.idDetailsUser,req.body.idLoginUser).then((response) => {
        res.status(200).send({ message: 'As informações foram inseridas com sucesso' });
    }).catch((err) => {
        res.status(500).send({ message: 'Houve um erro ao inserir as informações', err });
    })

}