import JWT from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import AuthModel from '../models/AuthModel';
import config from '../config/config';

/**
 * Esta função é utilizada para realizar o Login dos usuários.
 *
 * Esta função recebe o email e a senha do usuário, procura o e-mail na base de dados,
 * se encontrar compara a hash dos passwords
 * e retorna um Token criptografado com o ID e o nivel de Acesso
 * @param {*} req
 * @param {*} res
 */
class AuthController {
	static Login(req, res) {
		if(!req.body.email || !req.body.password){
			res.status(406).send({message: 'Informe os parametros devidamente.'});

			return;
		} 
		AuthModel.findUser(req.body.email).then((response) => {
			if(response){
				if (bcrypt.compareSync(req.body.password, response.password_user)) {
					AuthModel.selectUser(response.idLoginUser).then((responseUser) => {
						if (responseUser){
							const token = JWT.sign({
								idDetails: responseUser.idDetailsUser,
								stateUser: responseUser.idAccessStateUser,
								idLoginUser: responseUser.idLoginUser
							}, config.secret, { expiresIn: 60 * 10000 });

							res.status(200).send({ auth: true, token });
						} else {
							const token = JWT.sign({
								idLoginUser: response.idLoginUser
							}, config.secret, { expiresIn: 60 * 10000 });
							
							res.status(417).send({ auth: true, token });
						}
					}).catch((error) => {
						res.status(500).send({ message: 'Ocorreu um erro ao realizar o Login.', error });
						console.log(error);
					});

				} else {
					res.status(401).send({ auth: false, token: null });
				}
			} else {
				res.status(404).send({ auth: false, token: null });
			}
		}).catch((error) => {
			res.status(500).send({ message: 'Ocorreu um erro ao realizar o Login.', error });
			console.log(error);
		});
	}
}

export default AuthController;
