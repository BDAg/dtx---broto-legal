import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PainelfinanceiroPageRoutingModule } from './painelfinanceiro-routing.module';

import { PainelfinanceiroPage } from './painelfinanceiro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PainelfinanceiroPageRoutingModule
  ],
  declarations: [PainelfinanceiroPage]
})
export class PainelfinanceiroPageModule {}
