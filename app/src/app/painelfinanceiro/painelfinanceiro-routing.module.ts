import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PainelfinanceiroPage } from './painelfinanceiro.page';

const routes: Routes = [
  {
    path: '',
    component: PainelfinanceiroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PainelfinanceiroPageRoutingModule {}
