import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public navCtrl: NavController) { }

  goClients(){
    this.navCtrl.navigateForward("clientes")
  }

  goPedidos(){
    this.navCtrl.navigateForward("pedidos")
  }

  goPainelFinanceiro(){
    this.navCtrl.navigateForward("painelfinanceiro")
  }  

  goReceitas(){
    this.navCtrl.navigateForward("painelfinanceiro")
  } 

  ngOnInit() {
  }

}
