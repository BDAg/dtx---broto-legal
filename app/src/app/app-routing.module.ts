import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./login/home.module').then( m => m.HomePageModule)},
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'clientes', loadChildren: () => import('./clientes/clientes.module').then( m => m.ClientesPageModule)},
  { path: 'cadastrocliente', loadChildren: () => import('./cadastrocliente/cadastrocliente.module').then( m => m.CadastroclientePageModule)},
  { path: 'pedidos', loadChildren: () => import('./pedidos/pedidos.module').then( m => m.PedidosPageModule)},
  { path: 'adicionarpedido', loadChildren: () => import('./adicionarpedido/adicionarpedido.module').then( m => m.AdicionarpedidoPageModule)},
  { path: 'confirmarpedido', loadChildren: () => import('./confirmarpedido/confirmarpedido.module').then( m => m.ConfirmarpedidoPageModule)},
  { path: 'acompanharpedidos', loadChildren: () => import('./acompanharpedidos/acompanharpedidos.module').then( m => m.AcompanharpedidosPageModule)},
  { path: 'pedidosfinalizados', loadChildren: () => import('./pedidosfinalizados/pedidosfinalizados.module').then( m => m.PedidosfinalizadosPageModule)},
  { path: 'painelfinanceiro', loadChildren: () => import('./painelfinanceiro/painelfinanceiro.module').then( m => m.PainelfinanceiroPageModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

