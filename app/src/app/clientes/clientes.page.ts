import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  goCadastroCLI(){
    this.navCtrl.navigateForward('cadastrocliente');
  }

  ngOnInit() {
  }

}
