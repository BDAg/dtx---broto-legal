import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidosfinalizadosPageRoutingModule } from './pedidosfinalizados-routing.module';

import { PedidosfinalizadosPage } from './pedidosfinalizados.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosfinalizadosPageRoutingModule
  ],
  declarations: [PedidosfinalizadosPage]
})
export class PedidosfinalizadosPageModule {}
