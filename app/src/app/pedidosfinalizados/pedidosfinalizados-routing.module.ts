import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidosfinalizadosPage } from './pedidosfinalizados.page';

const routes: Routes = [
  {
    path: '',
    component: PedidosfinalizadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidosfinalizadosPageRoutingModule {}
