import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcompanharpedidosPageRoutingModule } from './acompanharpedidos-routing.module';

import { AcompanharpedidosPage } from './acompanharpedidos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcompanharpedidosPageRoutingModule
  ],
  declarations: [AcompanharpedidosPage]
})
export class AcompanharpedidosPageModule {}
