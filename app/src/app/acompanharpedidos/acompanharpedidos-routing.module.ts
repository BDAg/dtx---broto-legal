import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcompanharpedidosPage } from './acompanharpedidos.page';

const routes: Routes = [
  {
    path: '',
    component: AcompanharpedidosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcompanharpedidosPageRoutingModule {}
