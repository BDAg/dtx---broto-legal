import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AcompanharpedidosPage } from './acompanharpedidos.page';

describe('AcompanharpedidosPage', () => {
  let component: AcompanharpedidosPage;
  let fixture: ComponentFixture<AcompanharpedidosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcompanharpedidosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AcompanharpedidosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
