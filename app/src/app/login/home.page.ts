import { Component } from '@angular/core';
import axios from 'axios';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public navCtrl: NavController) {}
  email:string;
  pass:string;
  logIn(){
    let these = this
    axios({
      method: 'post',
      url: 'http://localhost:8080/v1/auth',
      data: {
        email:this.email,//mail,
        password: this.pass//passwd
      }
    }).then(function (response) {
      these.navCtrl.navigateForward('home');
    }).catch(function (error) {
      console.log(error)
      
    });    
  }
  
}
