import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdicionarpedidoPage } from './adicionarpedido.page';

const routes: Routes = [
  {
    path: '',
    component: AdicionarpedidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdicionarpedidoPageRoutingModule {}
