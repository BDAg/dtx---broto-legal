import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdicionarpedidoPageRoutingModule } from './adicionarpedido-routing.module';

import { AdicionarpedidoPage } from './adicionarpedido.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdicionarpedidoPageRoutingModule
  ],
  declarations: [AdicionarpedidoPage]
})
export class AdicionarpedidoPageModule {}
